FROM mongo

RUN mkdir -p build

WORKDIR /build

COPY . .

RUN chmod 777 init.sh

ENTRYPOINT [ "bash", "init.sh" ]