#!/bin/bash

db=${MONGO_DB:-admin}
user=${MONGO_USER:-root}
pw=${MONGO_PW:-root}
port=${MONGO_PORT:-27017}
host=${MONGO_HOST:-db}

uri="mongodb://$user:$pw@$host:$port/$db"

echo "URI: $uri"

echo "Starting Processing"
echo "Checking counts"
mongo_query='db.getCollection("ref_data").count({})'
count=`mongo --host="$uri" < counts.js --quiet`

echo "COUNTS: $count"

if [[ $count -eq 0 ]]; then
    echo "Creating collections"
    mongoimport --uri "$uri" --collection ref_data --file ref_data.json
    mongo --host="$uri" < init_ref_data.js --quiet
fi

echo "End Processing"